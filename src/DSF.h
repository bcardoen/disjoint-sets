/*
 * DSF.h
 *  This file holds an implementation of a disjoint data set forest.
 *  Created on: 4 Jul 2015
 *      Author: Ben Cardoen
 */

#ifndef DSF_H_
#define DSF_H_

#include<vector>
#include<functional>


/**
 * Disjoint set forest implementation based on Intr. to Alg. CLRS 3rd ed.
 * Uses path compression and union-by-rank heuristics to achieve O(m alpha(n)) complexity for m operations with n makeset(x)
 * and alpha the inverse Ackerman.
 * Treenodes have 2 extra fields to allow fast traversal of all nodes in a set, this adds only to the constant terms in the make/union/find operations.
 */

/**
 * Treenode in a disjoint set forest.
 * Use by pointer only.
 * Note that MakeSet(x) is equivalent to Node(x).
 */
template<typename Item>
class Node{
	/**
	 * If parent == this, this node is the root/representative node.
	 */
	Node*		parent;

	/**
	 * Pointer to next node allows traversal of a set.
	 */
	Node*		next;

	/**
	 * Tail pointer to last element of this tree, avoids breaking complexity of union.
	 * A tail pointer node is the first node in a tree which has itself as next node, storing
	 */
	Node*		last;

	/**
	 * Fixup list pointers in merge of subtrees.
	 * @see link
	 */
	void
	mergetrees(Node* subtreeroot){
		if(this->next == this){	/* This node is a leaf */
			this->next = subtreeroot;
			this->last = subtreeroot->last;
		}else{
			this->last->next=subtreeroot;
			this->last=subtreeroot->last;
		}
	}

	/**
	 * Link the sets defined by roots x, y of two disjoint sets.
	 * @complexity O(1)
	 */
	static
	void link(Node* x, Node* y){
		if(x->rank > y->rank){
			y->parent = x;
			x->mergetrees(y);
		}
		else{
			x->parent=y;
			y->mergetrees(x);
			if(x->rank==y->rank)
				++(y->rank);
		}
	}

	static
	Node* findset_recursive(Node* x){
		if(x->parent != x)
			x->parent = findset_recursive(x->parent);
		return x->parent;
	}

	static
	Node* findset_iterative(Node* x){
		std::vector<Node*> path;
		while(x->parent != x){
			path.push_back(x);
			x = x->parent;
		}
		for(auto& p : path)
			p->parent=x;
		return x;
	}

public:

	Item		data;

	/**
	 * Rank is proportional to the size of the tree. With the path compression heuristic, rank reflects
	 * the nr of subtrees merged into this root's tree.
	 * Used by link( , ) to limit growth of tree at merge.
	 */
	std::size_t	rank;

	typedef 	Item	item_type;

	Node(const Item& i):parent(this),next(this),last(this),data(i),rank(0){;}

	/**
	 * Traverse the disjoint set containing node, beginning at the root, applying funct on each node.
	 * @complexity O(n)
	 */
	static
	void traverse(Node* node, std::function<void(Node*)> f){
		Node* current = findset_recursive(node);
		while(current->next != current){	/* The 'tail' node is the only node in a tree for which holds next==this. */
			f(current);
			current = current->next;
		}
		f(current);
	}

	/**
	 * Find the representative of node x.
	 * Compresses the path from x to its root while searching.
	 */
	static
	Node* findset(Node* x){
		return findset_recursive(x);
	}

	/**
	 * Merge the sets containing x, y.
	 * If x,y belong to the same set, a merge is noop but the paths from x -> z , y -> z are still compressed.
	 */
	static
	void union_set(Node* x, Node* y){
		Node* lr = findset_recursive(x);
		Node* rr = findset_recursive(y);
		if(lr != rr)
			link(lr, rr);
	}

	/**
	 * Return true if sets containing x, y are disjoint.
	 * @note path compression is used during this operation, @see findset
	 */
	static
	bool disjoint(Node* x, Node* y){
		return(findset_recursive(x) != findset_recursive(y));
	}

	static
	bool is_repr(Node* x){
		return x->parent==x;
	}
};

void testDSF();

#endif /* DSF_H_ */
