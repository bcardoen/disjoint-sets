/*
 * Graph.h
 *
 *  Created on: 5 Jul 2015
 *      Author: Ben Cardoen
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include <unordered_map>
#include <list>

// Forward declare edge for use in vertex.
template<typename VertexId, typename EdgeLabel>
struct Edge;

template<typename VertexId, typename EdgeLabel>
struct Vertex{
	VertexId	id;
	Vertex(VertexId i):id(i){;}
	typedef		VertexId		vertex_id_type;
	std::list<Edge<VertexId, EdgeLabel>*> 	adjacency_list;
};

template<typename VertexId, typename EdgeLabel>
struct Edge{
	typedef EdgeLabel edge_label_type;
	EdgeLabel	label;
	Vertex<VertexId, EdgeLabel>*	to_node;
	Edge(EdgeLabel lbl, Vertex<VertexId, EdgeLabel>* v ):label(lbl),to_node(v){;}
};

template<typename VertexId, typename EdgeLabel>
class Graph{
private:
	std::unordered_map<VertexId, Vertex<VertexId, EdgeLabel>*> vertices;
public:
	void
	addEdge(VertexId from, EdgeLabel on, VertexId to){
		Vertex<VertexId, EdgeLabel>* source;
		auto found = vertices.find(from);
		if(vertices.find(from)==vertices.end()){
			source = new Vertex<VertexId, EdgeLabel>(from);
			vertices[from]=source;
		}else{
			source = found->second;
		}
		Vertex<VertexId, EdgeLabel>* sink;
		found = vertices.find(to);
		if(found==vertices.end()){
			sink = new Vertex<VertexId, EdgeLabel>(to);
			vertices[to]=sink;
		}else{
			sink = found->second;
		}
		source->adjacency_list.push_back(new Edge<VertexId, EdgeLabel>(on, sink));
	}
};

#endif /* GRAPH_H_ */
