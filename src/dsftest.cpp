/*

 * dsftest.cpp
 *
 *  Created on: 4 Jul 2015
 *      Author: Ben Cardoen
 */

#include "DSF.h"
#include <vector>
#include <iostream>

void testDSF(){
	constexpr size_t limit = 128;
	constexpr size_t stepsize = 16;
	std::vector<Node<int>*> nodes;
	for(std::size_t i = 0; i<limit; ++i){
		nodes.push_back(new Node<int>(i));
	}
    
	/* Create limit/stepsize disjoint sets. */
	for(std::size_t i = 0; i<=limit-stepsize; i+=stepsize){
		for(size_t j = i; j< (stepsize-1)+i;++j){
			Node<int>::union_set(nodes[j], nodes[j+1]);
		}
	}

	std::function<void(Node<int>*)> f = [](Node<int>* np)->void{
		std::cout << np->data << '\t';
	};

	for(size_t k = 0; k<limit; k+=stepsize){
		std::cout << " Set " << k << std::endl;
		Node<int>::traverse(nodes[k], f);
		std::cout << std::endl;
	}

	for(auto n : nodes){
		delete n;
	}

}
